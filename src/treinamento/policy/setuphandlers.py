# -*- coding:utf-8 -*-

from Products.CMFPlone.utils import _createObjectByType
from plone import api

from Products.CMFPlone.interfaces import INonInstallable
from zope.interface import implementer

import logging


default_profile = 'profile-treinamento.site:default'
logger = logging.getLogger(__name__)


@implementer(INonInstallable)
class HiddenProfiles(object):

    def getNonInstallableProfiles(self):
        """Hide uninstall profile from site-creation and quickinstaller"""
        return [
            'treinamento.policy:uninstall',
        ]


def post_install(context):
    """Post install script"""
    portal = portal = api.portal.get()

    deletePloneFolders(portal)
    createFolderStructure(portal)


def uninstall(context):
    """Uninstall script"""
    # Do something at the end of the uninstallation of this package.


def deletePloneFolders(portal):
    """Delete the standard Plone stuff that we don't need
    """
    # Delete standard Plone stuff..
    existing = portal.objectIds()
    itemsToDelete = ['Members', 'news', 'events']
    for item in itemsToDelete:
        if item in existing:
            portal.manage_delObjects(item)

def createFolderStructure(portal):
    """Define which objects we want to create in the site.
    """

    profile_children = [
        {   'id': 'receita-banners', 
            'title': 'Banners da área Receita do Portal',
            'description': '',
            'layout': 'folder_listing',
            'type': 'Folder',
            },
        ]

    top_folders = [
        {   'id': 'acesso-a-informacao', 
            'title': 'Acesso a Informação',
            'description': '',
            'layout': 'folder_summary_view',
            'type': 'Folder',
            },

        {   'id': 'fale-conosco', 
            'title': 'Fale Conosco',
            'description': '',
            'layout': '@@contact-info',
            'type': 'Folder',
            },

        {   'id': 'conf-portal', 
            'title': 'Configurações do Portal',
            'description': '',
            'layout': 'folder_listing',
            'type': 'Folder',
            'children': profile_children,
            },

        


        ]
    createObjects(parent=portal, children=top_folders)

def createObjects(parent, children):
    """This will create new objects, or modify existing ones if id's and type
    match.
    """
    logger.info("Creating %s in %s" % (children, parent))
    existing = parent.objectIds()
    logger.info("Existing ids: %s" % existing)
    for new_object in children:
        if new_object['id'] in existing:
            logger.info("%s exists, skipping" % new_object['id'])
        else:
            _createObjectByType(new_object['type'], parent, \
                id=new_object['id'], title=new_object['title'], \
                description=new_object['description'])
        logger.info("Now to modify the new_object...")
        obj = parent.get(new_object['id'], None)
        if obj is None:
            logger.info("can't get new_object %s to modify it!" % new_object['id'])
        else:
            if obj.Type() != new_object['type']:
                logger.info("types don't match!")
            else:
                obj.setLayout(new_object['layout'])
                obj.reindexObject()
                children = new_object.get('children',[])
                if len(children) > 0:
                    createObjects(obj, children)
